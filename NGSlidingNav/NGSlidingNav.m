//
//  NGSlidingNav.m
//  NGSlidingNav
//
//  Created by Nicholas Galasso on 12/9/12.
/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nick@rockshassa.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return. -NG
 * ----------------------------------------------------------------------------
 */

#import "NGSlidingNav.h"

@implementation NGSlidingNav

+(void)buildRelationshipWithParent:(UIViewController<NGSlidingNav>*)parentVC Child:(UIViewController*)childVC{
    
    [self buildRelationshipWithParent:parentVC Child:childVC defaultSwipe:YES];
}

+(void)buildRelationshipWithParent:(UIViewController<NGSlidingNav>*)parentVC Child:(UIViewController*)childVC defaultSwipe:(BOOL)swipe{
    
    //save a pointer to parent's view in _contentView;
    parentVC.contentView = parentVC.view;
    
    //save a pointer to the child we're adding
    parentVC.childVC = childVC;
    
    //reset self.view in parent to create our view hierarchy
    parentVC.view = [[UIView alloc] init];
    parentVC.view.autoresizesSubviews = parentVC.contentView.autoresizesSubviews;
    parentVC.view.frame = parentVC.contentView.frame;
    
    [parentVC.view addSubview:parentVC.contentView];
    
    if (swipe) {
        //add swipe gesture recognizer
        UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:[NGSlidingNav class] action:@selector(toggleSlide:)];
        [parentVC.contentView addGestureRecognizer:swipe];
    }
    
    //create the parent/child relationship
    [parentVC addChildViewController:childVC];
    [parentVC.view addSubview:childVC.view];
    
    //make callback
    [childVC didMoveToParentViewController:parentVC];
    
    //prepare for display
    [parentVC.view sendSubviewToBack:childVC.view];
    parentVC.sliderIsOpen = NO;
    parentVC.isToggling = NO;
    
}

+(void)toggleSlide:(id)sender{
    
    UIViewController<NGSlidingNav>* parent;
    
    if ([sender conformsToProtocol:@protocol(NGSlidingNav)]){
        parent = (UIViewController<NGSlidingNav>*)sender;
    } else {
        parent = [self findViewController:sender];
    }
    
    //see if we're allowed to toggle;
    if ([parent respondsToSelector:@selector(sliderShouldToggle)]){
        //bail out if parent returns NO
        if (![parent sliderShouldToggle]) return;
    }
    
    //if we're already toggling, ignore touches
    if (parent.isToggling) {
        return;
    } else {
        parent.isToggling = YES;
    }
    
    //let the parent know whats coming
    if ([parent respondsToSelector:@selector(sliderWillToggle)])[parent sliderWillToggle];
    
    NSTimeInterval animationDuration = .5;
    
    //make a completion block for after the animation
    void (^animationCompletedCallback)(void)= ^{
        parent.sliderIsOpen = !parent.sliderIsOpen;
        parent.isToggling = NO;
        if ([parent respondsToSelector:@selector(sliderDidToggle)])[parent sliderDidToggle];
    };
    
    if (parent.sliderIsOpen) {
        
        [UIView animateWithDuration:animationDuration
                         animations:^(void){
                             
                             [parent.blackTint removeFromSuperview];
                             parent.contentView.center = CGPointMake(parent.contentView.center.x - parent.childVC.view.frame.size.width, parent.contentView.center.y);
                             
                         }
                         completion:^(BOOL finished){
                             parent.blackTint = nil;
                             animationCompletedCallback();
                         }];
        
    } else {
        
        parent.blackTint = [[UIView alloc] initWithFrame:parent.contentView.frame];
        parent.blackTint.backgroundColor = [UIColor blackColor];
        parent.blackTint.alpha = 0;
        parent.blackTint.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [parent.contentView addSubview:parent.blackTint];
        
        [UIView animateWithDuration:animationDuration
                         animations:^(void){
                             
                             parent.blackTint.alpha = .5;
                             parent.contentView.center = CGPointMake(parent.contentView.center.x + parent.childVC.view.frame.size.width, parent.contentView.center.y);
                             
                         }
                         completion:^(BOOL finished){
                             
                             UITapGestureRecognizer *tintTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:[NGSlidingNav class] action:@selector(toggleSlide:)];
                             [parent.blackTint addGestureRecognizer:tintTapGesture];
                             
                             UISwipeGestureRecognizer *tintSwipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:[NGSlidingNav class] action:@selector(toggleSlide:)];
                             tintSwipeGesture.direction = UISwipeGestureRecognizerDirectionLeft;
                             [parent.blackTint addGestureRecognizer:tintSwipeGesture];
                             
                             animationCompletedCallback();
                         }];
    }
}

/*
 this is called to get a reference to parent VC from a sender.
 this code is based on http://stackoverflow.com/questions/1340434/get-to-uiviewcontroller-from-uiview-on-iphone
 */
+(UIViewController<NGSlidingNav>*)findViewController:(id)sender {
    
    UIResponder *responder;
    
    //if this is called from a gesture recognizer, get its containing view
    if ([sender respondsToSelector:@selector(view)]){
        
        responder = [(UIGestureRecognizer*)sender view];
        
    } else {
        //if not, assume it is a UIResponder subclass
        responder = sender;
    }
    
    UIViewController<NGSlidingNav> *vc = nil;
    
    while ((responder = [responder nextResponder])){
        if ([responder conformsToProtocol:@protocol(NGSlidingNav)]){
            vc = (UIViewController<NGSlidingNav>*)responder;
        }
    }
    return vc;
}

@end
