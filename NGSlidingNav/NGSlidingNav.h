//
//  NGSlidingNav.h
//  NGSlidingNav
//
//  Created by Nicholas Galasso on 12/9/12.
/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nick@rockshassa.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return. -NG
 * ----------------------------------------------------------------------------
 */
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

//Parent View Controller must conform to in order to implement this framework.
@protocol NGSlidingNav <NSObject>

@property (nonatomic, strong) UIViewController *childVC;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIView *blackTint;
@property (nonatomic, assign) BOOL sliderIsOpen;
@property (nonatomic, assign) BOOL isToggling;

@optional
/*
 callbacks sent to parent view controller before/after slider animation.
 handle conditional behavior by checking self.sliderIsOpen
 */
-(void)sliderWillToggle;
-(void)sliderDidToggle;

/*
 called before performing a toggle. return NO to disable toggle.
 */
-(BOOL)sliderShouldToggle;

@end

@interface NGSlidingNav : NSObject

/*
 Call this method in viewDidLoad in your parent View Controller. The child view controller must be non-nil.
 The child view controller is responsible for setting its view's frame to an appropriate width.
 Both view controllers must handle rotation on their own.
 */
+(void)buildRelationshipWithParent:(UIViewController<NGSlidingNav>*)parentVC Child:(UIViewController*)childVC;

/*
 Same as above but with option for no default swipe gesture.
 */
+(void)buildRelationshipWithParent:(UIViewController<NGSlidingNav>*)parentVC Child:(UIViewController*)childVC defaultSwipe:(BOOL)swipe;

/*
 Call this from a subview of your parent view controller (or from the parent itself) if you would like to initiate the slide in some other way.
 Ex. addTarget:[NGSlidingNav class] action:@selector(toggleSlide:)
 */
+(void)toggleSlide:(id)sender;

@end
